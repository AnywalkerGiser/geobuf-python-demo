# -*- coding: utf-8 -*-
# @Time    : 2022/3/20 15:26
# @Author  : dingsl
# @FileName: geobuf_demo.py
# @Software: PyCharm
# @CSDN: https://blog.csdn.net/qq_24655701

import tornado.httpserver
import tornado.ioloop
import tornado.options
from tornado.web import RequestHandler
import json, geobuf

from tornado.options import define,options

define('port', default=30050, help='run port', type=int)


class CommonHandler(RequestHandler):
    def set_default_headers(self):
        # 解决跨域问题
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT')
        self.set_header("Access-Control-Allow-Headers", "token, content-type, user-token")


class TestHandler(CommonHandler):
    def get(self):
        filepath = self.get_argument("filepath")
        geojson = json.load(open(filepath, encoding="utf-8"))
        buf = geobuf.encode(geojson)

        # 返回byte字符
        self.write(buf)


application = tornado.web.Application(
    handlers = [
        (r'/geobuf', TestHandler),
    ],

    # 是否开启debug模式，开启后保存修改内容会重新启动服务
    debug = False
)


if __name__ == '__main__':
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(options.port)
    print('http://127.0.0.1:{0}'.format(options.port))
    tornado.ioloop.IOLoop.instance().start()