# geobuf-python-demo

#### 介绍
Python版本的geobuf使用示例，使用tornado开发rest服务，将geojson文件转换为geobuf（byte）字符串，可使用js解析并前端展示

#### 关键技术
1. Rest框架：tornado
2. GeoJson压缩技术：geobuf